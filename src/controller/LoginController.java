
package controller;

import gestion_ventes.Gestion_ventes;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

/**
 *
 * @author mehdi
 */
public class LoginController {
    
 @FXML
    private TextField emailField;
    @FXML
    private PasswordField passwordField;
    @FXML
    private Label errorLabel;
    private Object inventoryService;

    @FXML
    protected void btnLoginPressed(ActionEvent event) {
        User user = inventoryService.getUserByEmail(emailField.getText());
        if (user.getPassword().equals(passwordField.getText())) {
            Gestion_ventes.showPage("dashboard");
        } else {
            errorLabel.setVisible(true);
        }
    }

    private static class User {

        public User() {
        }

        private Object getPassword() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }
}
