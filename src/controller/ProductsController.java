
package controller;

import gestion_ventes.Gestion_ventes;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 *
 * @author mehdi
 */
public class ProductsController {
    
@FXML
    private TableView<Product> tblProducts;
    @FXML
    private TableColumn<Product, String> idCol;
    @FXML
    private TableColumn<Product, String> productCol;
    @FXML
    private TableColumn<Product, String> groupCol;
    @FXML
    private TableColumn<Product, String> priceCol;

    @FXML
    private Button btnAddProd;

    public void initialize(URL url, ResourceBundle rb) {


        ObservableList<Product> tblData = FXCollections.observableArrayList(
                inventoryService.getProducts()
        );


        idCol.setCellValueFactory(new PropertyValueFactory<>("id"));
        productCol.setCellValueFactory(new PropertyValueFactory<>("name"));
        groupCol.setCellValueFactory(new PropertyValueFactory<>("group"));
        priceCol.setCellValueFactory(new PropertyValueFactory<>("price"));

        tblProducts.setItems(tblData);
    }// all()

    @FXML
    protected void handleAddProd() {
        // Since we are adding product, set selected ID to null
        Gestion_ventes.getInstance().repository.put("selectedProductId", null);
        Gestion_ventes.showPage("newProduct");
    }

    @FXML
    protected void openProduct() {
        Product prod = tblProducts.getSelectionModel().getSelectedItem();
        if (prod != null) {
            Gestion_ventes.getInstance().repository.put("selectedProductId", prod.getId().toString());
            Gestion_ventes.showPage("newProduct");
        }
    }

    private static class Product {

        public Product() {
        }

        private Object getId() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }

    private static class inventoryService {

        public inventoryService() {
        }
    }

}

