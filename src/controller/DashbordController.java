
package controller;

import java.net.URL;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;


public class DashbordController {
    
  @FXML
    private Label productsNumber;
    @FXML
    private Label groupsNumber;
    @FXML
    private Label ordersNumber;

    @FXML
    private BarChart<String, Number> ordersChart;
    @FXML
    private CategoryAxis x;
    @FXML
    private NumberAxis y;

    private Map<String, String> stats = inventoryService.getStats();

    public void initialize(URL url, ResourceBundle rb) {

        productsNumber.setText(stats.get("products_number"));
        groupsNumber.setText(stats.get("groups_number"));
        ordersNumber.setText(stats.get("orders_number"));

        _createBarChart();

    }

    private void _createBarChart() {

        Integer month1 = Integer.parseInt(stats.get("orders1"));
        Integer month2 = Integer.parseInt(stats.get("orders2"));
        Integer month3 = Integer.parseInt(stats.get("orders3"));
        Integer month4 = Integer.parseInt(stats.get("orders4"));

        XYChart.Series<String, Number> dataSet1 = new XYChart.Series<>();
        dataSet1.getData().addAll(
                new XYChart.Data<>(_getRecentMonthName(3), month1)
                , new XYChart.Data<>(_getRecentMonthName(2), month2)
                , new XYChart.Data<>(_getRecentMonthName(1), month3)
                , new XYChart.Data<>(_getRecentMonthName(0), month4)
        );

        ordersChart.getData().addAll(dataSet1);
    }

    private String _getRecentMonthName(int minusMonths) {

        DateTimeFormatter formatter = DateTimeFormat.forPattern("MMM"); // MMM = Jan,Feb...

        return formatter.print(LocalDate.now().minusMonths(minusMonths));
    }

    private static class inventoryService {

        private static Map<String, String> getStats() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        public inventoryService() {
        }
    }

    private static class DateTimeFormat {

        private static DateTimeFormatter forPattern(String mmm) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        public DateTimeFormat() {
        }
    }

    private static class LocalDate {

        private static Object now() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        public LocalDate() {
        }
    }


} // class
