/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import gestion_ventes.Gestion_ventes;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import models.GroupVariant;
import models.Order;
import models.OrderItem;
import models.ProductGroup;

/**
 *
 * @author mehdi
 */
public class AddEditOrderController {
    
 private static final String STATUS_IN_PROGRESS = "in-progress";
    private static final String STATUS_COMPLETED = "completed";


    @FXML
    private TextField fldName;
    @FXML
    private TextField fldCity;
    @FXML
    private TextField fldAddress;
    @FXML
    private TextField fldZip;
    @FXML
    private TextArea fldComment;
    @FXML
    private ComboBox<String> comboStatus;
    @FXML
    private ComboBox<String> comboType;

    @FXML
    private ComboBox<Product> comboProducts;
    @FXML
    private ComboBox<GroupVariant> comboVariants;

    @FXML
    private TableView<OrderItem> tblItems;
    @FXML
    private TableColumn<Product, String> colProduct;
    @FXML
    private TableColumn<GroupVariant, String> colVariant;
    @FXML
    private TableColumn<OrderItem, String> colPrice;

    @FXML
    private Label errorLabel;
   

    // Based on this value, we know if this is adding or editing page...
    private final String _orderId = Gestion_ventes.getInstance().repository.get("selectedOrderId");
    private Order editingOrder;

    private final List<OrderItem> removeList = new ArrayList<>();

    public void initialize(URL url, ResourceBundle rb) {

        _fillAllProductsCombo();
        comboStatus.getItems().addAll(STATUS_IN_PROGRESS, STATUS_COMPLETED);

        comboType.getItems().addAll("sell", "buy");

        tblItems.setEditable(true);

        colProduct.setCellValueFactory(new PropertyValueFactory<>("product"));
        colVariant.setCellValueFactory(new PropertyValueFactory<>("groupVariant"));
        colPrice.setCellValueFactory(new PropertyValueFactory<>("price"));
        colPrice.setCellFactory(TextFieldTableCell.<OrderItem>forTableColumn());

        colPrice.setOnEditCommit((TableColumn.CellEditEvent<OrderItem, String> t) -> {
            ((OrderItem) t.getTableView().getItems()
                    .get(t.getTablePosition().getRow()))
                    .setPrice(t.getNewValue());
        });

        if (_orderId != null) {
            _loadOrderData(_orderId);
        }

    }

    private void _loadOrderData(String orderId) {

        Order order = inventoryService.getOrder(orderId);

        fldName.setText(order.getName());
        fldCity.setText(order.getCity());
        fldAddress.setText(order.getAddress());
        fldZip.setText(order.getZip());
        fldComment.setText(order.getComment());

        comboStatus.getSelectionModel().select(order.getStatus());

        String selectedType = order.getType().equals("buy") ? "Buy" : "Sell";

        comboType.getSelectionModel().select(selectedType);

        tblItems.getItems().addAll(order.getItems());
        this.editingOrder = order;
    }

    private void _fillAllProductsCombo() {
        comboProducts.getItems().addAll(
                inventoryService.getProducts()
        );
        comboProducts.getSelectionModel().selectFirst();
    }

    private void _fillVariantsCombo(Product product) {

        ProductGroup group = product.getGroup();
        comboVariants.getItems().clear();
        comboVariants.getItems().addAll(
                group.getGroupVariants()
        );
        comboVariants.getSelectionModel().selectFirst();
    }

    // Fired when product in the combo is selected:
    @FXML
    private void productSelected() {
        if (comboProducts.getSelectionModel().getSelectedItem() instanceof Product == false) {
            System.out.println("Value in product combo is not Product model");
            return;
        }
        Product selectedProduct = comboProducts.getSelectionModel().getSelectedItem();

        _fillVariantsCombo(selectedProduct);
    }

    // Fired when Add button is clicked
    @FXML
    private void addProduct() {

        // If no product is selected, just skip this action
        if (comboProducts.getSelectionModel().getSelectedItem() instanceof Product == false) {
            System.out.println("Value in product combo is not Product model");
            return;
        }

        Product product = comboProducts.getSelectionModel().getSelectedItem();
        GroupVariant variant = comboVariants.getSelectionModel().getSelectedItem();

        if (product != null) {
            OrderItem item = new OrderItem();

            item.setProduct(product);
            item.setGroupVariant(variant);

            item.setOrder(this.editingOrder); // Ensures saving of orderitem

            tblItems.getItems().add(item);
            comboProducts.getSelectionModel().selectFirst();
            comboVariants.getItems().clear();
        }
    }

    @FXML
    protected boolean save() {

        Order order = new Order();

        if (this.editingOrder == null) {
            order.setCreated(DateTimeFormatter.ofPattern("yyyy-MM-dd").format(LocalDate.now()));
        } else {
            order = this.editingOrder;
        }

        order.setName(fldName.getText());
        order.setType(comboType.getSelectionModel().getSelectedItem().toLowerCase());
        order.setAddress(fldAddress.getText());
        order.setCity(fldCity.getText());
        order.setZip(fldZip.getText());
        order.setStatus(comboStatus.getSelectionModel().getSelectedItem());
        order.setComment(fldComment.getText());
        order.setItems(tblItems.getItems());

        if (inventoryService.saveOrder(order)) {
            this.removeItems();
            Gestion_ventes.showPage("orders");
        } else {
            System.out.println("Can't save order!");
            errorLabel.setVisible(true);
        }

        return true;
    }

    private void removeItems() {
        this.removeList.forEach((item) -> {
            inventoryService.removeOrderItem(item);
     });
    }

    @FXML
    private void handleBack() {
        Gestion_ventes.showPage("orders");
    }

    @FXML
    private void handleRemove() {

        OrderItem selectedItem = tblItems.getSelectionModel().getSelectedItem();

        if (selectedItem != null) {
            tblItems.getItems().removeAll(selectedItem);
            this.removeList.add(selectedItem);
        }
    }

    private static class Product {

        public Product() {
        }

        private ProductGroup getGroup() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }

    private static class inventoryService {

        private static Order getOrder(String orderId) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        private static Product[] getProducts() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        private static boolean saveOrder(Order order) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        private static void removeOrderItem(OrderItem item) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        public inventoryService() {
        }
    }

   

}//class
