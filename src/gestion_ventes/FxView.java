/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestion_ventes;

import java.io.IOException;
import java.net.URL;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.layout.Pane;

/**
 *
 * @author mehdi
 */
public class FxView {

 private Pane fxElement = null;

    public FxView(String fileName){

        try {
            URL fileUrl = Gestion_ventes.class.getResource("/views/" + fileName + ".fxml");
            if(fileUrl == null){
                throw new java.io.FileNotFoundException("FXML file can't be found");
            }
            fxElement = FXMLLoader.load(fileUrl);
        } catch (IOException e) {
            System.out.println(e.getMessage());
            // System.out.println(e.printStackTrace());
//            e.printStackTrace();
        }
    }

    public Pane get(){
        return fxElement;
    }


}

