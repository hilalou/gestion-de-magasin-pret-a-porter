
package gestion_ventes;

import java.util.List;
import javafx.scene.layout.Pane;


class FxPageSwitcher {

  private List<FxPage> pages;

	private String currentPage;

	private final IMainPane pane;

	public FxPageSwitcher(IMainPane pane, List<FxPage> pages){
	    this.pane = pane;
	    this.pages = pages;
    }

	public void showPage(String page){

	    try {
            FxPage selectedPage = pages.stream()
                    .filter((pg) -> pg.getPageName() == page)
                    .findFirst()
                    .get();

            if(selectedPage == null){
                System.out.println("Can't find page " + page);
                return ;
            }
            String viewFile = selectedPage.getPageFile();
            Pane view = (Pane) new FxView(viewFile).get();
            pane.setPage(view);
        } catch (Exception e){
	        System.out.println("No page " + page + " please check FxPageSwitcher.");
        }
	}

    private static class IMainPane {

        public IMainPane() {
        }

        private void setPage(Pane view) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }
}

   
